package com.abctech.blog;

import android.support.test.filters.SmallTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.abctech.blog.activity.MainActivity;
import com.abctech.blog.controller.EntryController;
import com.abctech.blog.model.EntryModel;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import io.realm.RealmResults;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by Nawapan Arampibulkit on 11/15/2016 AD.
 */

@RunWith(AndroidJUnit4.class)
@SmallTest
public class EntryTester {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    private EntryController entryController;

    @Before
    public void initValidString() {
        // Specify a valid string.
        entryController = new EntryController(mActivityRule.getActivity());
        entryController.publishContent("TEST", "TEST", null, null);
        entryController.publishContent("ABC", "TEST", null, null);
    }

    @Test
    public void testSortByTitle_thenEntryAscendingTitleShouldFirst() throws InterruptedException {
        RealmResults<EntryModel> entries = entryController.queryEntry(EntryController.SORT_BY_TITLE);
        assertThat(entries.get(0).getTitle(), is("ABC"));
    }

}
