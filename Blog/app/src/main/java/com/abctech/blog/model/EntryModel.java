package com.abctech.blog.model;

import android.support.annotation.Nullable;

import org.parceler.Parcel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

@SuppressWarnings("unused")
@Parcel
public class EntryModel extends RealmObject {

    @PrimaryKey
    long id;

    String title;
    String content;

    @Nullable
    String imageUrl;

    long createdTimeMillis;

    public EntryModel() {}

    public EntryModel(long id, @Nullable String imageUrl, String title, String content, long createdTimeMillis) {
        this.id = id;
        this.imageUrl = imageUrl;
        this.title = title;
        this.content = content;
        this.createdTimeMillis = createdTimeMillis;
    }

    public long getId() {
        return id;
    }

    @Nullable
    public String getImageUrl() {
        return imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public String getDate(String format, String timezone) {
        if( createdTimeMillis == 0 ) {
            return "";
        }

        SimpleDateFormat sdf = new SimpleDateFormat(
                format == null ? "dd/MM/yy HH:mm" : format,
                Locale.getDefault()
        );

        if( timezone != null ) {
            sdf.setTimeZone(TimeZone.getTimeZone(timezone));
        }

        return sdf.format(new Date(createdTimeMillis));
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setImageUrl(@Nullable String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setContent(String content) {
        this.content = content;

        extractImageFromContent();
    }

    public String getContent() {
        return content;
    }

    public void setCreatedTimeMillis(long timeMillis) {
        createdTimeMillis = timeMillis;
    }

    private void extractImageFromContent() {
        String imgRegex = "<[iI][mM][gG][^>]+[sS][rR][cC]\\s*=\\s*['\"]([^'\"]+)['\"][^>]*>";

        Pattern p = Pattern.compile(imgRegex);
        Matcher m = p.matcher(content);

        if (m.find()) {
            imageUrl = m.group(1);
        }
    }
}
