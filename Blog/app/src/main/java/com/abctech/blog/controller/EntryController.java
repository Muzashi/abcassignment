package com.abctech.blog.controller;

import android.content.Context;

import com.abctech.blog.adapter.EntryAdapter;
import com.abctech.blog.model.EntryModel;

import io.realm.Realm;
import io.realm.RealmAsyncTask;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by Nawapan Arampibulkit on 11/15/2016 AD.
 */

public class EntryController {

    public static final int SORT_BY_DATE = 0;
    public static final int SORT_BY_TITLE = 1;

    private RealmAsyncTask mTransaction;
    private Realm mRealm;

    private int mSoringType = SORT_BY_DATE;

    public EntryController(Context context) {
        Realm.init(context);

        mRealm = Realm.getDefaultInstance();
    }

    public void publishContent(final String title, final String content,
                               Realm.Transaction.OnSuccess onSuccess,
                               Realm.Transaction.OnError onError) {
        final Number maxId = mRealm.where(EntryModel.class).max("id");

        mTransaction = mRealm.executeTransactionAsync(
                new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        int nextKey = 0;
                        if( maxId != null ) {
                            nextKey = maxId.intValue() + 1;
                        }

                        EntryModel entry = realm.createObject(EntryModel.class, nextKey);
                        entry.setTitle(title);
                        entry.setContent(content);
                        entry.setCreatedTimeMillis(System.currentTimeMillis());
                    }
                }, onSuccess, onError
        );
    }

    public void cancelTransaction() {
        if( mTransaction != null && !mTransaction.isCancelled() ) {
            mTransaction.cancel();
        }
    }

    public RealmResults<EntryModel> queryEntry() {
        return queryEntry(mSoringType);
    }

    public RealmResults<EntryModel> queryEntry(int sorting) {
        mSoringType = sorting;

        RealmResults<EntryModel> entries = mRealm.where(EntryModel.class).findAll();

        switch (sorting) {
            case SORT_BY_TITLE:
                entries = entries.sort("title", Sort.ASCENDING);
                break;
            case SORT_BY_DATE:
                entries = entries.sort("createdTimeMillis", Sort.ASCENDING);
                break;
        }

        return entries;
    }

}
