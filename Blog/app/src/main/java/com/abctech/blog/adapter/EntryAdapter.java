package com.abctech.blog.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.abctech.blog.R;
import com.abctech.blog.holder.EntryHolder;
import com.abctech.blog.model.EntryModel;

import io.realm.RealmResults;

/**
 * Created by Nawapan Arampibulkit on 11/14/2016 AD.
 */

public class EntryAdapter extends BaseAdapter {

    private RealmResults<EntryModel> entries;

    public void setEntries(RealmResults<EntryModel> entries) {
        this.entries = entries;

        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return entries == null ? 0 : entries.size();
    }

    @Override
    public Object getItem(int position) {
        return entries.get(position);
    }

    @Override
    public long getItemId(int position) {
        return entries.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        EntryHolder holder;

        if( convertView == null ) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(
                            R.layout.item_entry, parent, false
                    );

            holder = new EntryHolder(convertView);
        }
        else {
            holder = (EntryHolder) convertView.getTag();
        }

        holder.build((EntryModel) getItem(position));

        return convertView;
    }

}
