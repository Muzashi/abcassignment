package com.abctech.blog.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.abctech.blog.R;
import com.abctech.blog.adapter.EntryAdapter;
import com.abctech.blog.controller.EntryController;
import com.abctech.blog.model.EntryModel;
import com.abctech.blog.utility.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.list_entries) ListView mListEntries;
    @BindView(R.id.swipe_refresh_layout) SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.button_add_blog) View mButtonAddBlog;

    private EntryController mController;
    private EntryAdapter mAdapterEntry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        Realm.init(this);

        mController = new EntryController(this);
        mAdapterEntry = new EntryAdapter();

        mListEntries.setAdapter(mAdapterEntry);
        mListEntries.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                EntryModel model = (EntryModel) mAdapterEntry.getItem(position);
                startViewBlog(model);
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        mAdapterEntry.setEntries(mController.queryEntry());
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                }
        );

        mButtonAddBlog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startAddBlog();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        mAdapterEntry.setEntries(mController.queryEntry());
    }

    public void startAddBlog() {
        Intent intent = new Intent(this, BlogActivity.class);
        startActivity(intent);
    }

    private void startViewBlog(EntryModel model) {
        Intent intent = new Intent(this, BlogViewerActivity.class);
        intent.putExtra(Constants.EntryId, model.getId());
        startActivity(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sort_by_title:
                mAdapterEntry.setEntries(mController.queryEntry(EntryController.SORT_BY_TITLE));
                return true;

            case R.id.action_sort_by_date:
                mAdapterEntry.setEntries(mController.queryEntry(EntryController.SORT_BY_DATE));
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
