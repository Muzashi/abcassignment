package com.abctech.blog.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.abctech.blog.R;
import com.abctech.blog.model.EntryModel;
import com.abctech.blog.utility.Constants;
import com.onegravity.rteditor.RTEditText;
import com.onegravity.rteditor.RTManager;
import com.onegravity.rteditor.api.RTApi;
import com.onegravity.rteditor.api.RTMediaFactoryImpl;
import com.onegravity.rteditor.api.RTProxyImpl;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Nawapan Arampibulkit on 11/14/2016 AD.
 */

public class BlogViewerActivity extends RTEditorBaseActivity {

    @BindView(R.id.entry_view_title) TextView mTitle;
    @BindView(R.id.entry_view_date) TextView mDate;
    @BindView(R.id.rt_edit_content) RTEditText mContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog_viewer);
        ButterKnife.bind(this);

        Realm.init(this);

        String title = null;
        String content = null;
        String date = null;

        Intent intent = getIntent();
        long entryId = intent.getLongExtra(Constants.EntryId, 0);
        Realm realm = Realm.getDefaultInstance();
        RealmResults<EntryModel> model = realm.where(EntryModel.class).equalTo("id", entryId).findAll();
        if( model.size() > 0 ) {
            title = model.get(0).getTitle();
            date = model.get(0).getDate(null, null);
            content = model.get(0).getContent();
        }

        // initialize rich text manager
        RTApi rtApi = new RTApi(this, new RTProxyImpl(this), new RTMediaFactoryImpl(this, true));
        RTManager mRTManager = new RTManager(rtApi, savedInstanceState);
        mRTManager.registerEditor(mContent, true);

        mTitle.setText(title);
        mDate.setText(date);

        mContent.setRichTextEditing(true, content);
        mContent.setEnabled(false);
        mContent.setFocusable(false);
        mContent.setClickable(false);
    }

}
