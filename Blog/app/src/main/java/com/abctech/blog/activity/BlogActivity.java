package com.abctech.blog.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.abctech.blog.R;
import com.abctech.blog.controller.EntryController;
import com.onegravity.rteditor.RTEditText;
import com.onegravity.rteditor.RTManager;
import com.onegravity.rteditor.RTToolbar;
import com.onegravity.rteditor.api.RTApi;
import com.onegravity.rteditor.api.RTMediaFactoryImpl;
import com.onegravity.rteditor.api.RTProxyImpl;
import com.onegravity.rteditor.api.format.RTFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

/**
 * Created by Nawapan Arampibulkit on 11/14/2016 AD.
 */

public class BlogActivity extends RTEditorBaseActivity {

    @BindView(R.id.rte_toolbar_container) ViewGroup mToolbarContainer;
    @BindView(R.id.rte_toolbar_character) RTToolbar mRTToolbarCharacter;
    @BindView(R.id.rte_toolbar_paragraph) RTToolbar mRTToolbarParagraph;

    @BindView(R.id.edit_title) EditText mTitleField;
    @BindView(R.id.rt_edit_content) RTEditText mRTContentField;

    @BindView(R.id.button_publish) View mButtonPublish;

    private RTManager mRTManager;
    private EntryController mEntryController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // set theme
        setTheme(R.style.RTE_ThemeLight);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_add_blog);

        ButterKnife.bind(this);

        mEntryController = new EntryController(this);

        String title;
        String content;
        if (savedInstanceState == null) {
            Intent intent = getIntent();
            title = getStringExtra(intent, PARAM_TITLE);
            content = getStringExtra(intent, PARAM_CONTENT);
        } else {
            title = savedInstanceState.getString(PARAM_TITLE, "");
            content = savedInstanceState.getString(PARAM_CONTENT, "");
        }

        // initialize rich text manager
        RTApi rtApi = new RTApi(this, new RTProxyImpl(this), new RTMediaFactoryImpl(this, true));
        mRTManager = new RTManager(rtApi, savedInstanceState);

        mRTManager.registerToolbar(mToolbarContainer, mRTToolbarCharacter);
        mRTManager.registerToolbar(mToolbarContainer, mRTToolbarParagraph);

        if( title != null ) {
            mTitleField.setText(title);
        }

        mRTManager.registerEditor(mRTContentField, true);
        if( content != null ) {
            mRTContentField.setRichTextEditing(true, content);
        }

        mRTContentField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if( b ) {
                    mToolbarContainer.setVisibility(View.VISIBLE);
                }
                else {
                    mToolbarContainer.setVisibility(View.GONE);
                }
            }
        });

        mButtonPublish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String title = mTitleField.getText().toString();
                String content = mRTContentField.getText(RTFormat.HTML);

                mEntryController.publishContent(title, content,
                        new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {
                                showAlertDialog(getString(R.string.entry_publish), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        finish();
                                    }
                                });
                            }
                        },
                        new Realm.Transaction.OnError() {
                            @Override
                            public void onError(Throwable error) {
                                showAlertDialog(error.getLocalizedMessage(), null);
                            }
                        });
            }
        });
    }

    public void showAlertDialog(String string, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(string);
        builder.setPositiveButton(R.string.ok, listener);
        builder.create().show();
    }

    @Override
    public void onStop() {
        super.onStop();

        mEntryController.cancelTransaction();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mRTManager != null) {
            mRTManager.onDestroy(true);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        mRTManager.onSaveInstanceState(outState);

        String title = mTitleField.getText().toString();
        outState.putString(PARAM_TITLE, title);

        String content = mRTContentField.getText().toString();
        outState.putString(PARAM_CONTENT, content);
    }

}
