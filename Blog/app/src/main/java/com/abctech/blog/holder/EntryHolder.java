package com.abctech.blog.holder;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.abctech.blog.R;
import com.abctech.blog.model.EntryModel;
import com.bumptech.glide.Glide;

import java.io.File;

/**
 * Created by Nawapan Arampibulkit on 11/14/2016 AD.
 */

public class EntryHolder {

    private Context context;
    private ImageView image;
    private TextView title;
    private TextView date;

    public EntryHolder(View view) {
        view.setTag(this);

        context = view.getContext();

        title = (TextView) view.findViewById(R.id.entry_item_title);
        image = (ImageView) view.findViewById(R.id.entry_item_image);
        date = (TextView) view.findViewById(R.id.entry_item_date);
    }

    public void build(EntryModel model) {
        title.setText(model.getTitle());
        date.setText(model.getDate(null, null));

        String imageUrl = model.getImageUrl();
        if( imageUrl != null ) {
            if (imageUrl.startsWith("http")) {
                Glide.with(context)
                        .load(imageUrl)
                        .placeholder(R.drawable.loading_image)
                        .into(image);
            } else {
                Glide.with(context)
                        .load(new File(imageUrl))
                        .into(image);
            }
        }
        else {
            image.setImageResource(R.drawable.loading_image);
        }
    }

}
